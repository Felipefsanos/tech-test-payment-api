﻿using Application.Models;
using Application.Requests;
using AutoMapper;
using Domain.Entities;
using Infra.Helpers.ExtensionsMethods;

namespace Application.AutoMapper
{
    public static class AutoMapperConfig
    {
        public static void MapperConfig(IMapperConfigurationExpression config)
        {
            config.CreateMap<RegisterSaleRequest, Sale>();
            config.CreateMap<RegisterSellerRequest, Seller>();
            config.CreateMap<SaleItemRequest, Item>();

            config.CreateMap<Sale, SaleModel>()
                  .ForMember(s => s.Status, s => s.MapFrom(value => value.Status.GetCustomAttributeDescription()));
            config.CreateMap<Seller, SellerModel>();
            config.CreateMap<Item, SaleItemModel>();
        }
    }
}
