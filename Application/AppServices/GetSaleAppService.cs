﻿using Application.AppServices.Interfaces;
using Application.Models;
using AutoMapper;
using Domain.Services.Interfaces;

namespace Application.AppServices
{
    public class GetSaleAppService(IGetSaleService getSaleService, IMapper mapper) : IGetSaleAppService
    {
        public async Task<SaleModel> GetSale(Guid saleId, CancellationToken cancellationToken)
        {
            var sale = await getSaleService.GetSale(saleId, cancellationToken);
            
            return mapper.Map<SaleModel>(sale);
        }
    }
}
