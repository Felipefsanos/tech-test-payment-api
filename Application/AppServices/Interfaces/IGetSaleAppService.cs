﻿
using Application.Models;

namespace Application.AppServices.Interfaces
{
    public interface IGetSaleAppService
    {
        Task<SaleModel> GetSale(Guid saleId, CancellationToken cancellationToken);
    }
}
