﻿using Application.Models;

namespace Application.AppServices.Interfaces
{
    public interface IRegisterSaleAppService
    {
        Task<Guid> RegisterSale(RegisterSaleRequest request, CancellationToken cancellationToken);
    }
}
