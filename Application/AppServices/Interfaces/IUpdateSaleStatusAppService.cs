﻿using Application.Models;

namespace Application.AppServices.Interfaces
{
    public interface IUpdateSaleStatusAppService
    {
        Task<SaleModel> UpdateSaleStatus(Guid saleId, UpdateStatusRequest updateStatusModel, CancellationToken cancellationToken);
    }
}
