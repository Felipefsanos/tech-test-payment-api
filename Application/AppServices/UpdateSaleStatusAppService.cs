﻿using Application.AppServices.Interfaces;
using Application.Models;
using AutoMapper;
using Domain.Repositories.Base;
using Domain.Services.Interfaces;
using FluentValidation;

namespace Application.AppServices
{
    public class UpdateSaleStatusAppService(IValidator<UpdateStatusRequest> validator, IUpdateStatusService updateStatusService, 
                                            IMapper mapper, IUnitOfWork unitOfWork) : IUpdateSaleStatusAppService
    {
        public async Task<SaleModel> UpdateSaleStatus(Guid saleId, UpdateStatusRequest updateStatusModel, CancellationToken cancellationToken)
        {
            await ValidateNewStatus(updateStatusModel, cancellationToken);

            var sale = await updateStatusService.UpdateSaleStatus(saleId, updateStatusModel.Status, cancellationToken);

            await unitOfWork.SaveChanges();

            return mapper.Map<SaleModel>(sale);
        }

        private async Task ValidateNewStatus(UpdateStatusRequest updateStatusModel, CancellationToken cancellationToken)
        {
            var validationResult = await validator.ValidateAsync(updateStatusModel, cancellationToken);

            if (!validationResult.IsValid)
            {
                throw new ValidationException(validationResult.Errors);
            }
        }
    }
}
