﻿using Application.AppServices.Interfaces;
using Application.Models;
using AutoMapper;
using Domain.Entities;
using Domain.Repositories.Base;
using Domain.Services.Interfaces;
using FluentValidation;

namespace Application.AppServices
{
    public class RegisterSaleAppService(IValidator<RegisterSaleRequest> validator, IMapper mapper, 
                                        IRegisterSaleService registerSaleService, IUnitOfWork unitOfWork) : IRegisterSaleAppService
    {
        public async Task<Guid> RegisterSale(RegisterSaleRequest request, CancellationToken cancellationToken)
        {
            await ValidateRequest(request, cancellationToken);

            var sale = mapper.Map<Sale>(request);

            var newSaleId = await registerSaleService.RegisterSale(sale, cancellationToken);

            await unitOfWork.SaveChanges();

            return newSaleId;
        }

        private async Task ValidateRequest(RegisterSaleRequest request, CancellationToken cancellationToken)
        {
            var validationResult = await validator.ValidateAsync(request, cancellationToken);

            if (!validationResult.IsValid)
            {
                throw new ValidationException(validationResult.Errors);
            }
        }
    }
}
