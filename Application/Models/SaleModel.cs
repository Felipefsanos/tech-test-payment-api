﻿using Domain.Enums;

namespace Application.Models
{
    public class SaleModel
    {
        public Guid Id { get; set; }
        public SellerModel Seller { get; set; } = null!;
        public DateTime Date { get; set; } = DateTime.Now;
        public string Status { get; set; } = string.Empty;
        public IEnumerable<SaleItemModel> Items { get; set; } = Enumerable.Empty<SaleItemModel>();
    }
}
