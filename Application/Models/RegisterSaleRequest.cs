﻿using Application.Requests;

namespace Application.Models
{
    public class RegisterSaleRequest
    {
        public RegisterSellerRequest Seller { get; set; } = null!;
        public IEnumerable<SaleItemRequest> Items { get; set; } = Enumerable.Empty<SaleItemRequest>();
    }
}
