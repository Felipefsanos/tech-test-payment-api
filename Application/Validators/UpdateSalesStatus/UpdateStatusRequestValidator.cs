﻿using Application.Models;
using FluentValidation;

namespace Application.Validators.UpdateSalesStatus
{
    public class UpdateStatusRequestValidator : AbstractValidator<UpdateStatusRequest>
    {
        public UpdateStatusRequestValidator()
        {
            RuleFor(p => p.Status)
                .NotEmpty()
                .WithMessage("O status não pode ser vazio.")
                .Must(p => p.Trim().Equals("Pagamento aprovado".Trim(), StringComparison.CurrentCultureIgnoreCase)
                        || p.Trim().Equals("Enviado para transportadora".Trim(), StringComparison.CurrentCultureIgnoreCase)
                        || p.Trim().Equals("Entregue".Trim(), StringComparison.CurrentCultureIgnoreCase)
                        || p.Trim().Equals("Cancelada".Trim(), StringComparison.CurrentCultureIgnoreCase))
                .WithMessage("O status deve ser 'Pagamento aprovado' ou 'Enviado para transportadora' ou 'Entregue' ou 'Cancelada'.");
        }
    }
}
