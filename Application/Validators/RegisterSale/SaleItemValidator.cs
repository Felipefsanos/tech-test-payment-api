﻿using Application.Requests;
using FluentValidation;

namespace Application.Validators.RegisterSale
{
    public class SaleItemValidator : AbstractValidator<SaleItemRequest>
    {
        public SaleItemValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty()
                .WithMessage("Nome do item não pode ser nulo ou vazio.");
        }
    }
}
