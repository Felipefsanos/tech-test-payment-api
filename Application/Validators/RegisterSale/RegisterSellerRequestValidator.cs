﻿using Application.Requests;
using FluentValidation;

namespace Application.Validators.RegisterSale
{
    public class RegisterSellerRequestValidator : AbstractValidator<RegisterSellerRequest>
    {
        public RegisterSellerRequestValidator()
        {
            RuleFor(p => p.Email)
                .NotEmpty()
                .WithMessage("Email não pode ser nulo ou vazio.")
                .EmailAddress()
                .WithMessage("Deve ser informado um e-mail válido.");

            RuleFor(p => p.Cpf)
                .NotEmpty()
                .WithMessage("CPF não pode ser nulo ou vazio.")
                .MinimumLength(11)
                .WithMessage("CPF deve conter 11 dígitos");

            RuleFor(p => p.Name)
                .NotEmpty()
                .WithMessage("Nome náo pode ser nulo ou vazio.");

            RuleFor(p => p.Phone)
                .NotEmpty()
                .WithMessage("Telefone não pode ser nulo ou vazio.")
                .MinimumLength(10)
                .WithMessage("Informe apenas os números do telefone, deve conter no mínimo 10 dígitos");
        }
    }
}
