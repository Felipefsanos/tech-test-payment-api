﻿using Application.Models;
using Application.Requests;
using FluentValidation;

namespace Application.Validators.RegisterSale
{
    public class RegisterSaleRequestValidator : AbstractValidator<RegisterSaleRequest>
    {
        public RegisterSaleRequestValidator()
        {
            RuleFor(p => p.Seller)
                .NotNull()
                .WithMessage("Vendedor não pode ser nulo.")
                .SetValidator(new RegisterSellerRequestValidator());

            RuleFor(p => p.Items)
                .NotEmpty()
                .WithMessage("A venda deve conter pelo menos um item.");

            RuleForEach(p => p.Items)
                .SetValidator(new SaleItemValidator());
        }
    }
}
