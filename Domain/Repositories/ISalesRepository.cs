﻿using Domain.Entities;

namespace Domain.Repositories
{
    public interface ISalesRepository
    {
        Task<Sale?> GetSale(Guid saleId, CancellationToken cancellationToken);
        Task RegisterSale(Sale sale, CancellationToken cancellationToken);
        void UpdateSale(Sale sale);
    }
}
