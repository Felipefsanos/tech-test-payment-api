﻿namespace Domain.Repositories.Base
{
    public interface IUnitOfWork
    {
        Task SaveChanges();
    }
}
