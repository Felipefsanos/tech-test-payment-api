﻿using System.ComponentModel;

namespace Domain.Enums
{
    public enum SaleStatus
    {
        [Description("Aguardando Pagamento")]
        AguardandoPagamento,
        [Description("Pagamento Aprovado")]
        PagamentoAprovado,
        [Description("Cancelada")]
        Cancelada,
        [Description("Enviado para Transportadora")]
        EnviadoTransportadora,
        [Description("Entregue")]
        Entregue
    }
}
