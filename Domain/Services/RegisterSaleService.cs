﻿using Domain.Entities;
using Domain.Repositories;
using Domain.Services.Interfaces;

namespace Domain.Services
{
    public class RegisterSaleService(ISalesRepository salesRepository) : IRegisterSaleService
    {
        public async Task<Guid> RegisterSale(Sale? sale, CancellationToken cancellationToken)
        {
            ArgumentNullException.ThrowIfNull(sale);

            await salesRepository.RegisterSale(sale, cancellationToken);

            return sale.Id;
        }
    }
}
