﻿using Domain.Entities;
using Domain.Enums;
using Domain.Repositories;
using Domain.Services.Interfaces;
using Infra.Helpers.Exceptions;

namespace Domain.Services
{
    public class UpdateStatusService(ISalesRepository saleRepository) : IUpdateStatusService
    {
        public async Task<Sale> UpdateSaleStatus(Guid saleId, string status, CancellationToken cancellationToken)
        {
            var sale = await saleRepository.GetSale(saleId, cancellationToken) ?? throw new NotFoundException($"Sale not found for id {saleId}.");

            var saleStatus = ConvertStatus(status);

            ValidateStatusTransitions(sale, saleStatus);

            sale.UpdateStatus(saleStatus);

            saleRepository.UpdateSale(sale);

            return sale;
        }

        private static void ValidateStatusTransitions(Sale sale, SaleStatus saleStatus)
        {
            switch (saleStatus)
            {
                case SaleStatus.PagamentoAprovado when sale.Status != SaleStatus.AguardandoPagamento:
                    throw new ArgumentException("O Status da venda não pode ser atualizado para o status solicitado.");
                case SaleStatus.EnviadoTransportadora when sale.Status != SaleStatus.PagamentoAprovado:
                    throw new ArgumentException("O Status da venda não pode ser atualizado para o status solicitado.");
                case SaleStatus.Entregue when sale.Status != SaleStatus.EnviadoTransportadora:
                    throw new ArgumentException("O Status da venda não pode ser atualizado para o status solicitado.");
                case SaleStatus.Cancelada when (sale.Status != SaleStatus.AguardandoPagamento && sale.Status != SaleStatus.PagamentoAprovado):
                    throw new ArgumentException("O Status da venda não pode ser atualizado para o status solicitado.");
            }
        }

        private static SaleStatus ConvertStatus(string status) => status.ToLower().Trim() switch
        {
            "pagamento aprovado" => SaleStatus.PagamentoAprovado,
            "enviado para transportadora" => SaleStatus.EnviadoTransportadora,
            "entregue" => SaleStatus.Entregue,
            "cancelada" => SaleStatus.Cancelada,
            _ => throw new ArgumentException($"Status '{status}' não é um valor válido.")
        };
    }
}
