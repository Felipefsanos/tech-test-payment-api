﻿using Domain.Entities;
using Domain.Repositories;
using Domain.Services.Interfaces;
using Infra.Helpers.Exceptions;

namespace Domain.Services
{
    public class GetSaleService(ISalesRepository salesRepository) : IGetSaleService
    {
        public async Task<Sale> GetSale(Guid saleId, CancellationToken cancellationToken) 
            => await salesRepository.GetSale(saleId, cancellationToken) ?? throw new NotFoundException($"Sale not found for id: {saleId}");
    }
}
