﻿
using Domain.Entities;

namespace Domain.Services.Interfaces
{
    public interface IUpdateStatusService
    {
        Task<Sale> UpdateSaleStatus(Guid saleId, string status, CancellationToken cancellationToken);
    }
}
