﻿using Domain.Entities;

namespace Domain.Services.Interfaces
{
    public interface IGetSaleService
    {
        Task<Sale> GetSale(Guid saleId, CancellationToken cancellationToken);
    }
}
