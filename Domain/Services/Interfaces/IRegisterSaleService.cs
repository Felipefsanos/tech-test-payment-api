﻿using Domain.Entities;

namespace Domain.Services.Interfaces
{
    public interface IRegisterSaleService
    {
        Task<Guid> RegisterSale(Sale? sale, CancellationToken cancellationToken);
    }
}
