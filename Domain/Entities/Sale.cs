﻿using Domain.Enums;
using System.Collections.ObjectModel;

namespace Domain.Entities
{
    public class Sale
    {
        public Guid Id { get; set; }
        public Guid SellerId { get; set; }
        public Seller Seller { get; set; } = null!;
        public DateTime Date { get; set; } = DateTime.Now;
        public SaleStatus Status { get; set; }
        public ICollection<Item> Items { get; set; } = new Collection<Item>();

        public void UpdateStatus(SaleStatus saleStatus) => Status = saleStatus;
    }
}
