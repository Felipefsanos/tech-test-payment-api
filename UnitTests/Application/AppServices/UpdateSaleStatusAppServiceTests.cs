﻿using Application.AppServices;
using Application.Models;
using AutoFixture;
using AutoMapper;
using Domain.Entities;
using Domain.Repositories.Base;
using Domain.Services.Interfaces;
using FluentAssertions;
using FluentValidation;
using FluentValidation.Results;
using Moq;

namespace UnitTests.Application.AppServices
{
    public class UpdateSaleStatusAppServiceTests
    {
        private readonly Mock<IValidator<UpdateStatusRequest>> _validatorMock = new();
        private readonly Mock<IUpdateStatusService> _updateStatusServiceMock = new();
        private readonly Mock<IMapper> _mapperMock = new();
        private readonly Mock<IUnitOfWork> _unitOfWorkMock = new();
        private readonly Fixture _fixture = new();
        private readonly UpdateSaleStatusAppService _appService;

        public UpdateSaleStatusAppServiceTests() => _appService = new UpdateSaleStatusAppService(_validatorMock.Object, _updateStatusServiceMock.Object, _mapperMock.Object, _unitOfWorkMock.Object);

        [Fact]
        public async Task UpdateSaleStatus_WhenCalledWithInvalidStatus_ShouldThrowValidationException()
        {
            var saleId = Guid.NewGuid();
            var updateStatusModel = new UpdateStatusRequest();

            _validatorMock.Setup(x => x.ValidateAsync(updateStatusModel, It.IsAny<CancellationToken>()))
                          .ReturnsAsync(new ValidationResult(_fixture.CreateMany<ValidationFailure>()));

            await Assert.ThrowsAsync<ValidationException>(() => _appService.UpdateSaleStatus(saleId, updateStatusModel, It.IsAny<CancellationToken>()));
        }

        [Fact]
        public async Task UpdateSaleStatus_WhenHaveValidStatus_ShouldCallServiceAndSaveChanges()
        {
            var saleId = Guid.NewGuid();
            var updateStatusModel = new UpdateStatusRequest();
            var sale = _fixture.Build<Sale>().With(p => p.Id, saleId).Create();

            _validatorMock.Setup(x => x.ValidateAsync(updateStatusModel, It.IsAny<CancellationToken>()))
                          .ReturnsAsync(new ValidationResult());

            _updateStatusServiceMock.Setup(x => x.UpdateSaleStatus(saleId, updateStatusModel.Status, It.IsAny<CancellationToken>()))
                                    .ReturnsAsync(sale);
            _mapperMock.Setup(x => x.Map<SaleModel>(sale))
                       .Returns(_fixture.Build<SaleModel>().With(p => p.Id, saleId).Create());

            var result = await _appService.UpdateSaleStatus(saleId, updateStatusModel, It.IsAny<CancellationToken>());

            result.Id.Should().Be(saleId);
            _updateStatusServiceMock.Verify(x => x.UpdateSaleStatus(saleId, updateStatusModel.Status, It.IsAny<CancellationToken>()), Times.Once);
            _unitOfWorkMock.Verify(x => x.SaveChanges(), Times.Once);
        }

    }
}
