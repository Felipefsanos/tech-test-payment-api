﻿using Application.AppServices;
using Application.Models;
using AutoFixture;
using AutoMapper;
using Domain.Entities;
using Domain.Repositories.Base;
using Domain.Services.Interfaces;
using FluentAssertions;
using FluentValidation;
using FluentValidation.Results;
using Moq;

namespace UnitTests.Application.AppServices
{
    public class RegisterSaleAppServiceTests
    {
        private readonly Mock<IValidator<RegisterSaleRequest>> _validator = new();
        private readonly Mock<IMapper> _mapper = new();
        private readonly Mock<IRegisterSaleService> _registerSaleService = new();
        private readonly Mock<IUnitOfWork> _unitOfWork = new();
        private readonly Fixture _fixture = new();
        private readonly RegisterSaleAppService _appService;

        public RegisterSaleAppServiceTests() => _appService = new RegisterSaleAppService(_validator.Object, _mapper.Object, _registerSaleService.Object, _unitOfWork.Object);

        [Fact]
        public async Task RegisterSale_WhenHaveValidationError_ShouldThrowValidationException()
        {
            var request = _fixture.Create<RegisterSaleRequest>();

            _validator.Setup(x => x.ValidateAsync(request, It.IsAny<CancellationToken>()))
                      .ReturnsAsync(new ValidationResult(_fixture.CreateMany<ValidationFailure>()));

            await Assert.ThrowsAsync<ValidationException>(() => _appService.RegisterSale(request, It.IsAny<CancellationToken>()));
        }

        [Fact]
        public async Task RegisterSale_WhenCalledWithNoErrors_ShouldReturnNewSaleId()
        {
            var request = _fixture.Create<RegisterSaleRequest>();
            var sale = _fixture.Create<Sale>();

            _validator.Setup(x => x.ValidateAsync(request, It.IsAny<CancellationToken>()))
                      .ReturnsAsync(new ValidationResult());
            _mapper.Setup(x => x.Map<Sale>(request)).Returns(sale);
            _registerSaleService.Setup(x => x.RegisterSale(sale, It.IsAny<CancellationToken>())).ReturnsAsync(sale.Id);

            var result = await _appService.RegisterSale(request, It.IsAny<CancellationToken>());

            result.Should().Be(sale.Id);
            _unitOfWork.Verify(x => x.SaveChanges(), Times.Once);
        }
    }
}
