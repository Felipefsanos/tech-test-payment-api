﻿using Application.AppServices;
using Application.Models;
using AutoFixture;
using AutoMapper;
using Domain.Entities;
using Domain.Services.Interfaces;
using FluentAssertions;
using Moq;

namespace UnitTests.Application.AppServices
{
    public class GetSaleAppServiceTests
    {
        private readonly Mock<IGetSaleService> _getSaleService = new();
        private readonly Mock<IMapper> _mapper = new();
        private readonly Fixture _fixture = new();
        private readonly GetSaleAppService _appService;

        public GetSaleAppServiceTests() => _appService = new GetSaleAppService(_getSaleService.Object, _mapper.Object);

        [Fact]
        public async Task GetSale_WhenServiceThrowError_ShouldThrowException()
        {
            var sale = _fixture.Create<Sale>();
            var expectedSale = _fixture.Create<SaleModel>();
            var id = Guid.NewGuid();

            _getSaleService.Setup(x => x.GetSale(id, It.IsAny<CancellationToken>()))
                           .ThrowsAsync(new Exception());
            _mapper.Setup(x => x.Map<SaleModel>(sale)).Returns(expectedSale);

            await Assert.ThrowsAsync<Exception>(() => _appService.GetSale(id, It.IsAny<CancellationToken>()));
        }

        [Fact]
        public async Task GetSale_WhenCalled_ShouldReturnSuccess()
        {
            var sale = _fixture.Create<Sale>();
            var expectedSale = _fixture.Create<SaleModel>();
            var id = Guid.NewGuid();

            _getSaleService.Setup(x => x.GetSale(id, It.IsAny<CancellationToken>())).ReturnsAsync(sale);
            _mapper.Setup(x => x.Map<SaleModel>(sale)).Returns(expectedSale);

            var result = await _appService.GetSale(id, It.IsAny<CancellationToken>());

            result.Should().BeEquivalentTo(expectedSale);
        }
    }
}
