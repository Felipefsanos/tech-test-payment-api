﻿using Application.Requests;
using Application.Validators.RegisterSale;
using AutoFixture;
using FluentValidation.TestHelper;

namespace UnitTests.Application.Validators.RegisterSale
{
    public class SaleItemValidatorTests(SaleItemValidator validator) : IClassFixture<SaleItemValidator>
    {
        private readonly Fixture _fixture = new();

        [Fact]
        public void ShouldHaveErrorWhenItemNamesNull()
        {
            var request = _fixture.Build<SaleItemRequest>()
                                  .Without(p => p.Name)
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldHaveValidationErrorFor(p => p.Name)
                  .WithErrorMessage("Nome do item não pode ser nulo ou vazio.");
        }
    }
}
