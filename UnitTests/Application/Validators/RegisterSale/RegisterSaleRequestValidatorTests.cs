﻿using System.Net.Mail;
using Application.Models;
using Application.Requests;
using Application.Validators.RegisterSale;
using AutoFixture;
using FluentValidation.TestHelper;

namespace UnitTests.Application.Validators.RegisterSale
{
    public class RegisterSaleRequestValidatorTests(RegisterSaleRequestValidator validator)
        : IClassFixture<RegisterSaleRequestValidator>
    {
        private readonly Fixture _fixture = new();

        [Fact]
        public void ShouldHaveErrorWhenSellerIsNull()
        {
            var request = new RegisterSaleRequest();

            var result = validator.TestValidate(request);

            result.ShouldHaveValidationErrorFor(p => p.Seller)
                .WithErrorMessage("Vendedor não pode ser nulo.");
        }

        [Fact]
        public void ShouldHaveErrorWhenItemsIsEmpty()
        {
            var request = new RegisterSaleRequest
            {
                Seller = GetValidSaleRequest(),
                Items = Enumerable.Empty<SaleItemRequest>()
            };

            var result = validator.TestValidate(request);

            result.ShouldHaveValidationErrorFor(p => p.Items)
                .WithErrorMessage("A venda deve conter pelo menos um item.");
        }

        private RegisterSellerRequest GetValidSaleRequest()
        {
            return _fixture.Build<RegisterSellerRequest>()
                           .With(p => p.Email, _fixture.Create<MailAddress>().Address)
                           .Create();
        }
    }
}
