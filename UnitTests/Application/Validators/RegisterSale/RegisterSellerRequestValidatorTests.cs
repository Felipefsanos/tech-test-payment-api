﻿using Application.Requests;
using Application.Validators.RegisterSale;
using AutoFixture;
using FluentValidation.TestHelper;

namespace UnitTests.Application.Validators.RegisterSale
{
    public class RegisterSellerRequestValidatorTests(RegisterSellerRequestValidator validator)
        : IClassFixture<RegisterSellerRequestValidator>
    {
        private readonly Fixture _fixture = new();

        [Fact]
        public void ShouldHaveErrorWhenEmailIsNull()
        {
            var request = _fixture.Build<RegisterSellerRequest>()
                .Without(p => p.Email)
                .Create();

            var result = validator.TestValidate(request);

            result.ShouldHaveValidationErrorFor(p => p.Email)
                .WithErrorMessage("Email não pode ser nulo ou vazio.");
        }

        [Fact]
        public void ShouldHaveErrorWhenEmailIsEmpty()
        {
            var request = _fixture.Build<RegisterSellerRequest>()
                .With(p => p.Email, "")
                .Create();

            var result = validator.TestValidate(request);

            result.ShouldHaveValidationErrorFor(p => p.Email)
                .WithErrorMessage("Email não pode ser nulo ou vazio.");
        }

        [Fact]
        public void ShouldHaveErrorWhenEmailIsInvalid()
        {
            var request = _fixture.Build<RegisterSellerRequest>()
                                  .With(p => p.Email, "invalid-email")
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldHaveValidationErrorFor(p => p.Email)
                .WithErrorMessage("Deve ser informado um e-mail válido.");
        }

        [Fact]
        public void ShouldHaveErrorWhenCpfIsNull()
        {
            var request = _fixture.Build<RegisterSellerRequest>()
                                  .Without(p => p.Cpf)
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldHaveValidationErrorFor(p => p.Cpf)
                .WithErrorMessage("CPF não pode ser nulo ou vazio.");
        }

        [Fact]
        public void ShouldHaveErrorWhenCpfIsEmpty()
        {
            var request = _fixture.Build<RegisterSellerRequest>()
                                  .With(p => p.Cpf, "")
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldHaveValidationErrorFor(p => p.Cpf)
                .WithErrorMessage("CPF não pode ser nulo ou vazio.");
        }

        [Fact]
        public void ShouldHaveErrorWhenCpfIsLessThanElevenDigits()
        {
            var request = _fixture.Build<RegisterSellerRequest>()
                                  .With(p => p.Cpf, "1234567890")
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldHaveValidationErrorFor(p => p.Cpf)
                .WithErrorMessage("CPF deve conter 11 dígitos");
        }

        [Fact]
        public void ShouldHaveErrorWhenNameIsNull()
        {
            var request = _fixture.Build<RegisterSellerRequest>()
                                  .Without(p => p.Name)
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldHaveValidationErrorFor(p => p.Name)
                .WithErrorMessage("Nome náo pode ser nulo ou vazio.");
        }

        [Fact]
        public void ShouldHaveErrorWhenNameIsEmpty()
        {
            var request = _fixture.Build<RegisterSellerRequest>()
                                  .With(p => p.Name, "")
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldHaveValidationErrorFor(p => p.Name)
                .WithErrorMessage("Nome náo pode ser nulo ou vazio.");
        }

        [Fact]
        public void ShouldHaveErrorWhenPhoneIsNull()
        {
            var request = _fixture.Build<RegisterSellerRequest>()
                                  .Without(p => p.Phone)
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldHaveValidationErrorFor(p => p.Phone)
                .WithErrorMessage("Telefone não pode ser nulo ou vazio.");
        }

        [Fact]
        public void ShouldHaveErrorWhenPhoneIsEmpty()
        {
            var request = _fixture.Build<RegisterSellerRequest>()
                                  .With(p => p.Phone, "")
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldHaveValidationErrorFor(p => p.Phone)
                .WithErrorMessage("Telefone não pode ser nulo ou vazio.");
        }

        [Fact]
        public void ShouldHaveErrorWhenPhoneIsLessThanTenDigits()
        {
            var request = _fixture.Build<RegisterSellerRequest>()
                                  .With(p => p.Phone, "1234567")
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldHaveValidationErrorFor(p => p.Phone)
                .WithErrorMessage("Informe apenas os números do telefone, deve conter no mínimo 10 dígitos");
        }
    }
}
