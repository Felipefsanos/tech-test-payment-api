﻿using Application.Models;
using Application.Validators.UpdateSalesStatus;
using AutoFixture;
using FluentValidation.TestHelper;

namespace UnitTests.Application.Validators.UpdateSaleStatus
{
    public class UpdateSaleStatusRequestValidatorTests(UpdateStatusRequestValidator validator): IClassFixture<UpdateStatusRequestValidator>
    {
        private readonly Fixture _fixture = new();

        [Fact]
        public void ShouldHaveErrorWhenStatusIsNull()
        {
            var request = _fixture.Build<UpdateStatusRequest>()
                                  .Without(p => p.Status)
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldHaveValidationErrorFor(p => p.Status)
                  .WithErrorMessage("O status não pode ser vazio.");
        }

        [Fact]
        public void ShouldHaveErrorWhenStatusIsEmpty()
        {
            var request = _fixture.Build<UpdateStatusRequest>()
                                  .With(p => p.Status, "")
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldHaveValidationErrorFor(p => p.Status)
                  .WithErrorMessage("O status não pode ser vazio.");
        }

        [Fact]
        public void ShouldHaveErrorWhenStatusIsInvalid()
        {
            var request = _fixture.Build<UpdateStatusRequest>()
                                  .With(p => p.Status, "invalid-status")
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldHaveValidationErrorFor(p => p.Status)
                  .WithErrorMessage("O status deve ser 'Pagamento aprovado' ou 'Enviado para transportadora' ou 'Entregue' ou 'Cancelada'.");
        }

        [Fact]
        public void ShouldNotHaveErrorWhenStatusIsValid()
        {
            var request = _fixture.Build<UpdateStatusRequest>()
                                  .With(p => p.Status, "Pagamento aprovado")
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldNotHaveValidationErrorFor(p => p.Status);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenStatusIsValid2()
        {
            var request = _fixture.Build<UpdateStatusRequest>()
                                  .With(p => p.Status, "Enviado para transportadora")
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldNotHaveValidationErrorFor(p => p.Status);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenStatusIsValid3()
        {
            var request = _fixture.Build<UpdateStatusRequest>()
                                  .With(p => p.Status, "Entregue")
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldNotHaveValidationErrorFor(p => p.Status);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenStatusIsValid4()
        {
            var request = _fixture.Build<UpdateStatusRequest>()
                                  .With(p => p.Status, "Cancelada")
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldNotHaveValidationErrorFor(p => p.Status);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenStatusIsValid5()
        {
            var request = _fixture.Build<UpdateStatusRequest>()
                                  .With(p => p.Status, "cancelada")
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldNotHaveValidationErrorFor(p => p.Status);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenStatusIsValid6()
        {
            var request = _fixture.Build<UpdateStatusRequest>()
                                  .With(p => p.Status, "entregue ")
                                  .Create();

            var result = validator.TestValidate(request);

            result.ShouldNotHaveValidationErrorFor(p => p.Status);
        }
    }
}
