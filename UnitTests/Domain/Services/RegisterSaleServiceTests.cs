﻿using Domain.Entities;
using Domain.Repositories;
using Domain.Services;
using Moq;

namespace UnitTests.Domain.Services
{
    public class RegisterSaleServiceTests
    {
        private readonly Mock<ISalesRepository> _salesRepositoryMock = new();
        private readonly RegisterSaleService _service;

        public RegisterSaleServiceTests() => _service = new RegisterSaleService(_salesRepositoryMock.Object);

        [Fact]
        public async Task RegisterSale_WhenSaleIsValid_ReturnsSaleId()
        {
            // Arrange
            var sale = new Sale();
            _salesRepositoryMock.Setup(x => x.RegisterSale(sale, It.IsAny<CancellationToken>()));

            // Act
            var result = await _service.RegisterSale(sale, It.IsAny<CancellationToken>());

            // Assert
            Assert.Equal(sale.Id, result);
        }

        [Fact]
        public async Task RegisterSale_WhenSaleIsNull_ThrowsArgumentNullException()
        {
            // Arrange
            Sale? sale = null;

            // Act
            var exception = await Assert.ThrowsAsync<ArgumentNullException>(() => _service.RegisterSale(sale, It.IsAny<CancellationToken>()));

            // Assert
            Assert.Equal("Value cannot be null. (Parameter 'sale')", exception.Message);
        }
    }
}
