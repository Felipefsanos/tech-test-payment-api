﻿using AutoFixture;
using Domain.Entities;
using Domain.Enums;
using Domain.Repositories;
using Domain.Services;
using FluentAssertions;
using Infra.Helpers.Exceptions;
using Moq;

namespace UnitTests.Domain.Services
{
    public class UpdateStatusServiceTests
    {
        private readonly Mock<ISalesRepository> _salesRepositoryMock = new();
        private readonly UpdateStatusService _service;
        private readonly Fixture _fixture = new();

        public UpdateStatusServiceTests() => _service = new UpdateStatusService(_salesRepositoryMock.Object);

        [Fact]
        public async Task UpdateStatus_WhenSaleExistsUpdateStatus_AndReturnsSale()
        {
            // Arrange
            const string validStatus = "Pagamento Aprovado";
            var saleId = Guid.NewGuid();
            var sale = _fixture.Build<Sale>()
                               .With(p => p.Status, SaleStatus.AguardandoPagamento)
                               .Create();
            _salesRepositoryMock.Setup(x => x.GetSale(saleId, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            // Act
            var result = await _service.UpdateSaleStatus(saleId, validStatus, It.IsAny<CancellationToken>());

            // Assert
            result.Id.Should().Be(sale.Id);
            result.Status.Should().Be(SaleStatus.PagamentoAprovado);
            _salesRepositoryMock.Verify(x => x.UpdateSale(sale), Times.Once);
        }

        [Fact]
        public async Task UpdateStatus_WhenSaleDoesNotExist_ThrowsNotFoundException()
        {
            // Arrange
            const string validStatus = "Pagamento Aprovado";
            var saleId = Guid.NewGuid();
            _salesRepositoryMock.Setup(x => x.GetSale(saleId, It.IsAny<CancellationToken>())).ReturnsAsync((Sale?)null);

            // Act
            var exception = await Assert.ThrowsAsync<NotFoundException>(() => _service.UpdateSaleStatus(saleId, validStatus, It.IsAny<CancellationToken>()));

            // Assert
            Assert.Equal($"Sale not found for id {saleId}.", exception.Message);
        }

        [Fact]
        public async Task UpdateStatus_WhenStatusIsInvalid_ThrowsArgumentException()
        {
            // Arrange
            const string invalidStatus = "Status Inválido";
            var saleId = Guid.NewGuid();
            var sale = _fixture.Build<Sale>()
                               .With(p => p.Status, SaleStatus.AguardandoPagamento)
                               .Create();
            _salesRepositoryMock.Setup(x => x.GetSale(saleId, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            // Act
            var exception = await Assert.ThrowsAsync<ArgumentException>(() => _service.UpdateSaleStatus(saleId, invalidStatus, It.IsAny<CancellationToken>()));

            // Assert
            Assert.Equal($"Status '{invalidStatus}' não é um valor válido.", exception.Message);
        }

        [Fact]
        public async Task
            UpdateStatus_WhenStatusIsPagamentoAprovado_AndCurrentStatusIsAguardandoPagamento_UpdatesStatus()
        {
            // Arrange
            const string validStatus = "Pagamento Aprovado";
            var saleId = Guid.NewGuid();
            var sale = _fixture.Build<Sale>()
                               .With(p => p.Status, SaleStatus.AguardandoPagamento)
                               .Create();
            _salesRepositoryMock.Setup(x => x.GetSale(saleId, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            // Act
            var result = await _service.UpdateSaleStatus(saleId, validStatus, It.IsAny<CancellationToken>());

            // Assert
            result.Id.Should().Be(sale.Id);
            result.Status.Should().Be(SaleStatus.PagamentoAprovado);
            _salesRepositoryMock.Verify(x => x.UpdateSale(sale), Times.Once);
        }

        [Fact]
        public async Task
            UpdateStatus_WhenStatusIsEnviadoTransportadora_AndCurrentStatusIsPagamentoAprovado_UpdatesStatus()
        {
            // Arrange
            const string validStatus = "Enviado para Transportadora";
            var saleId = Guid.NewGuid();
            var sale = _fixture.Build<Sale>()
                               .With(p => p.Status, SaleStatus.PagamentoAprovado)
                               .Create();
            _salesRepositoryMock.Setup(x => x.GetSale(saleId, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            // Act
            var result = await _service.UpdateSaleStatus(saleId, validStatus, It.IsAny<CancellationToken>());

            // Assert
            result.Id.Should().Be(sale.Id);
            result.Status.Should().Be(SaleStatus.EnviadoTransportadora);
            _salesRepositoryMock.Verify(x => x.UpdateSale(sale), Times.Once);
        }

        [Fact]
        public async Task UpdateStatus_WhenStatusIsEntregue_AndCurrentStatusIsEnviadoTransportadora_UpdatesStatus()
        {
            // Arrange
            const string validStatus = "Entregue";
            var saleId = Guid.NewGuid();
            var sale = _fixture.Build<Sale>()
                               .With(p => p.Status, SaleStatus.EnviadoTransportadora)
                               .Create();
            _salesRepositoryMock.Setup(x => x.GetSale(saleId, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            // Act
            var result = await _service.UpdateSaleStatus(saleId, validStatus, It.IsAny<CancellationToken>());

            // Assert
            result.Id.Should().Be(sale.Id);
            result.Status.Should().Be(SaleStatus.Entregue);
            _salesRepositoryMock.Verify(x => x.UpdateSale(sale), Times.Once);
        }

        [Fact]
        public async Task UpdateStatus_WhenStatusIsCancelada_AndCurrentStatusIsAguardandoPagamento_UpdatesStatus()
        {
            // Arrange
            const string validStatus = "Cancelada";
            var saleId = Guid.NewGuid();
            var sale = _fixture.Build<Sale>()
                               .With(p => p.Status, SaleStatus.AguardandoPagamento)
                               .Create();
            _salesRepositoryMock.Setup(x => x.GetSale(saleId, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            // Act
            var result = await _service.UpdateSaleStatus(saleId, validStatus, It.IsAny<CancellationToken>());

            // Assert
            result.Id.Should().Be(sale.Id);
            result.Status.Should().Be(SaleStatus.Cancelada);
            _salesRepositoryMock.Verify(x => x.UpdateSale(sale), Times.Once);
        }

        [Fact]
        public async Task UpdateStatus_WhenStatusIsCancelada_AndCurrentStatusIsPagamentoAprovado_UpdatesStatus()
        {
            // Arrange
            const string validStatus = "Cancelada";
            var saleId = Guid.NewGuid();
            var sale = _fixture.Build<Sale>()
                               .With(p => p.Status, SaleStatus.PagamentoAprovado)
                               .Create();
            _salesRepositoryMock.Setup(x => x.GetSale(saleId, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            // Act
            var result = await _service.UpdateSaleStatus(saleId, validStatus, It.IsAny<CancellationToken>());

            // Assert
            result.Id.Should().Be(sale.Id);
            result.Status.Should().Be(SaleStatus.Cancelada);
            _salesRepositoryMock.Verify(x => x.UpdateSale(sale), Times.Once);
        }

        [Fact]
        public async Task UpdateStatus_WhenStatusIsPagamentoAprovado_AndCurrentStatusIsCancelada_ThrowsArgumentException()
        {
            // Arrange
            const string invalidStatus = "Pagamento Aprovado";
            var saleId = Guid.NewGuid();
            var sale = _fixture.Build<Sale>()
                               .With(p => p.Status, SaleStatus.Cancelada)
                               .Create();
            _salesRepositoryMock.Setup(x => x.GetSale(saleId, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            // Act
            var exception = await Assert.ThrowsAsync<ArgumentException>(() => _service.UpdateSaleStatus(saleId, invalidStatus, It.IsAny<CancellationToken>()));

            // Assert
            Assert.Equal("O Status da venda não pode ser atualizado para o status solicitado.", exception.Message);
        }

        [Fact]
        public async Task UpdateStatus_WhenStatusIsEnviadoTransportadora_AndCurrentStatusIsAguardandoPagamento_ThrowsArgumentException()
        {
            // Arrange
            const string invalidStatus = "Enviado para Transportadora";
            var saleId = Guid.NewGuid();
            var sale = _fixture.Build<Sale>()
                               .With(p => p.Status, SaleStatus.AguardandoPagamento)
                               .Create();
            _salesRepositoryMock.Setup(x => x.GetSale(saleId, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            // Act
            var exception = await Assert.ThrowsAsync<ArgumentException>(() => _service.UpdateSaleStatus(saleId, invalidStatus, It.IsAny<CancellationToken>()));

            // Assert
            Assert.Equal("O Status da venda não pode ser atualizado para o status solicitado.", exception.Message);
        }

        [Fact]
        public async Task UpdateStatus_WhenStatusIsEnviadoTransportadora_AndCurrentStatusIsEntregue_ThrowsArgumentException()
        {
            // Arrange
            const string invalidStatus = "Enviado para Transportadora";
            var saleId = Guid.NewGuid();
            var sale = _fixture.Build<Sale>()
                               .With(p => p.Status, SaleStatus.Entregue)
                               .Create();
            _salesRepositoryMock.Setup(x => x.GetSale(saleId, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            // Act
            var exception = await Assert.ThrowsAsync<ArgumentException>(() => _service.UpdateSaleStatus(saleId, invalidStatus, It.IsAny<CancellationToken>()));

            // Assert
            Assert.Equal("O Status da venda não pode ser atualizado para o status solicitado.", exception.Message);
        }

        [Fact]
        public async Task UpdateStatus_WhenStatusIsEntregue_AndCurrentStatusIsAguardandoPagamento_ThrowsArgumentException()
        {
            // Arrange
            const string invalidStatus = "Entregue";
            var saleId = Guid.NewGuid();
            var sale = _fixture.Build<Sale>()
                               .With(p => p.Status, SaleStatus.AguardandoPagamento)
                               .Create();
            _salesRepositoryMock.Setup(x => x.GetSale(saleId, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            // Act
            var exception = await Assert.ThrowsAsync<ArgumentException>(() => _service.UpdateSaleStatus(saleId, invalidStatus, It.IsAny<CancellationToken>()));

            // Assert
            Assert.Equal("O Status da venda não pode ser atualizado para o status solicitado.", exception.Message);
        }

        [Fact]
        public async Task UpdateStatus_WhenStatusIsEntregue_AndCurrentStatusIsCancelada_ThrowsArgumentException()
        {
            // Arrange
            const string invalidStatus = "Entregue";
            var saleId = Guid.NewGuid();
            var sale = _fixture.Build<Sale>()
                               .With(p => p.Status, SaleStatus.Cancelada)
                               .Create();
            _salesRepositoryMock.Setup(x => x.GetSale(saleId, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            // Act
            var exception = await Assert.ThrowsAsync<ArgumentException>(() => _service.UpdateSaleStatus(saleId, invalidStatus, It.IsAny<CancellationToken>()));

            // Assert
            Assert.Equal("O Status da venda não pode ser atualizado para o status solicitado.", exception.Message);
        }

        [Fact]
        public async Task UpdateStatus_WhenStatusIsCancelada_AndCurrentStatusIsEntregue_ThrowsArgumentException()
        {
            // Arrange
            const string invalidStatus = "Cancelada";
            var saleId = Guid.NewGuid();
            var sale = _fixture.Build<Sale>()
                               .With(p => p.Status, SaleStatus.Entregue)
                               .Create();
            _salesRepositoryMock.Setup(x => x.GetSale(saleId, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            // Act
            var exception = await Assert.ThrowsAsync<ArgumentException>(() => _service.UpdateSaleStatus(saleId, invalidStatus, It.IsAny<CancellationToken>()));

            // Assert
            Assert.Equal("O Status da venda não pode ser atualizado para o status solicitado.", exception.Message);
        }

        [Fact]
        public async Task
            UpdateStatus_WhenStatusIsCancelada_AndCurrentStatusIsEnviadoTransportadora_ThrowsArgumentException()
        {
            // Arrange
            const string invalidStatus = "Cancelada";
            var saleId = Guid.NewGuid();
            var sale = _fixture.Build<Sale>()
                               .With(p => p.Status, SaleStatus.EnviadoTransportadora)
                               .Create();
            _salesRepositoryMock.Setup(x => x.GetSale(saleId, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            // Act
            var exception = await Assert.ThrowsAsync<ArgumentException>(() => _service.UpdateSaleStatus(saleId, invalidStatus, It.IsAny<CancellationToken>()));

            // Assert
            Assert.Equal("O Status da venda não pode ser atualizado para o status solicitado.", exception.Message);
        }
    }
}
