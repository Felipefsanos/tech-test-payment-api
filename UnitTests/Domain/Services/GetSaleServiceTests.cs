﻿using Domain.Entities;
using Domain.Repositories;
using Domain.Services;
using Infra.Helpers.Exceptions;
using Moq;

namespace UnitTests.Domain.Services
{
    public class GetSaleServiceTests
    {
        private readonly Mock<ISalesRepository> _salesRepositoryMock = new();
        private readonly GetSaleService _service;

        public GetSaleServiceTests() => _service = new GetSaleService(_salesRepositoryMock.Object);

        [Fact]
        public async Task GetSale_WhenSaleExists_ReturnsSale()
        {
            // Arrange
            var saleId = Guid.NewGuid();
            var sale = new Sale { Id = saleId };
            _salesRepositoryMock.Setup(x => x.GetSale(saleId, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            // Act
            var result = await _service.GetSale(saleId, It.IsAny<CancellationToken>());

            // Assert
            Assert.Equal(sale, result);
        }

        [Fact]
        public async Task GetSale_WhenSaleDoesNotExist_ThrowsNotFoundException()
        {
            // Arrange
            var saleId = Guid.NewGuid();
            _salesRepositoryMock.Setup(x => x.GetSale(saleId, It.IsAny<CancellationToken>())).ReturnsAsync((Sale?)null);

            // Act
            var exception = await Assert.ThrowsAsync<NotFoundException>(() => _service.GetSale(saleId, It.IsAny<CancellationToken>()));

            // Assert
            Assert.Equal($"Sale not found for id: {saleId}", exception.Message);
        }
    }
}
