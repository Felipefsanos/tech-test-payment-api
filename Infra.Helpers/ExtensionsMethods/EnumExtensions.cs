﻿using System.ComponentModel;

namespace Infra.Helpers.ExtensionsMethods
{
    public static class EnumExtensions
    {
        public static string? GetCustomAttributeDescription(this Enum value)
        {
            var attr = value.GetType().GetField(value.ToString())?.GetCustomAttributes(typeof(DescriptionAttribute), false).FirstOrDefault() as DescriptionAttribute;
            return attr?.Description;
        }
    }
}
