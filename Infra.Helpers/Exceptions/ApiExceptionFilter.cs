﻿using FluentValidation;
using FluentValidation.Results;
using Infra.Helpers.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;

namespace Infra.Helpers.Exceptions
{
    public class ApiExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            var exception = context.Exception;

            if (exception is ValidationException)
            {
                var validationException = exception as ValidationException;

                var statusCode = (int)HttpStatusCode.BadRequest;

                context.Result = new ObjectResult(new { StatusCode = statusCode, Message = FormatValidationErrors(validationException!.Errors) })
                {
                    StatusCode = statusCode
                };
            }

            if (exception is ArgumentNullException)
            {
                var argumentNullException = exception as ArgumentNullException;

                var statusCode = (int)HttpStatusCode.BadRequest;

                var formatedError = new ErrorModel(statusCode, argumentNullException!.Message);

                context.Result = new ObjectResult(formatedError)
                {
                    StatusCode = statusCode
                };
            }

            if (exception is ArgumentException)
            {
                var argumentNullException = exception as ArgumentException;

                var statusCode = (int)HttpStatusCode.BadRequest;

                var formatedError = new ErrorModel(statusCode, argumentNullException!.Message);

                context.Result = new ObjectResult(formatedError)
                {
                    StatusCode = statusCode
                };
            }

            if (exception is NotFoundException)
            {
                var notFoundException = exception as NotFoundException;

                var statusCode = (int)HttpStatusCode.NotFound;

                var formatedError = new ErrorModel(statusCode, notFoundException!.Message);

                context.Result = new ObjectResult(formatedError)
                {
                    StatusCode = statusCode
                };
            }
        }

        private IDictionary<string, IList<string>> FormatValidationErrors(IEnumerable<ValidationFailure> errors)
        {
            var formattedErrors = new Dictionary<string, IList<string>>();

            foreach (var error in errors)
            {
                if (!formattedErrors.ContainsKey(error.PropertyName))
                {
                    formattedErrors[error.PropertyName] = new List<string>();
                }

                formattedErrors[error.PropertyName].Add(error.ErrorMessage);
            }

            return formattedErrors;
        }
    }
}
