using Infra.Helpers.Exceptions;
using Infra.IoC;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
DependencyInjector.ConfigureDependencies(builder.Services, builder.Configuration);
builder.Services.AddControllers(opt => opt.Filters.Add(typeof(ApiExceptionFilter)));
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger(option =>
    {
        option.RouteTemplate = "api-docs/{documentName}/swagger.json";
    });
    app.UseSwaggerUI(opt =>
    {
        opt.SwaggerEndpoint("/api-docs/v1/swagger.json", "API");
        opt.RoutePrefix = "api-docs";
    });
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
