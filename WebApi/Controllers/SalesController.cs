﻿using Application.AppServices.Interfaces;
using Application.Models;
using Application.Requests;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/sales")]
    [ApiController]
    public class SalesController(IRegisterSaleAppService registerSaleAppService, 
                                 IGetSaleAppService getSaleAppService, IUpdateSaleStatusAppService updateSaleStatusAppService) : ControllerBase
    {
        private readonly IRegisterSaleAppService _registerSaleAppService = registerSaleAppService;
        private readonly IGetSaleAppService _getSaleAppService = getSaleAppService;
        private readonly IUpdateSaleStatusAppService _updateSaleStatusAppService = updateSaleStatusAppService;

        [HttpPost]
        public async Task<IActionResult> RegisterSale([FromBody] RegisterSaleRequest request, CancellationToken cancellationToken)
        {
            return Ok(await _registerSaleAppService.RegisterSale(request, cancellationToken));
        }

        [HttpGet("{saleId}")]
        public async Task<IActionResult> GetSale(Guid saleId, CancellationToken cancellationToken)
        {
            return Ok(await _getSaleAppService.GetSale(saleId, cancellationToken));
        }

        [HttpPatch("{saleId}")]
        public async Task<IActionResult> UpdateSaleStatus(Guid saleId, [FromBody] UpdateStatusRequest updateStatusModel, CancellationToken cancellationToken)
        {
            return Ok(await _updateSaleStatusAppService.UpdateSaleStatus(saleId, updateStatusModel, cancellationToken));
        }
    }
}
