﻿using Application.AppServices;
using Application.AppServices.Interfaces;
using Application.AutoMapper;
using Application.Models;
using Application.Requests;
using Application.Validators.RegisterSale;
using Application.Validators.UpdateSalesStatus;
using Domain.Repositories;
using Domain.Repositories.Base;
using Domain.Services;
using Domain.Services.Interfaces;
using FluentValidation;
using Infra.Data.Context;
using Infra.Data.Repositories;
using Infra.Data.Repositories.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infra.IoC
{
    public static class DependencyInjector
    {
        public static IServiceCollection ConfigureDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            return ConfigureDataBase(services, configuration)
                  .ConfigureAutoMapper()
                  .ConfigureValidators()
                  .ConfigureRepositories()
                  .ConfigureServices()
                  .ConfigureAppServices();
        }

        public static IServiceCollection ConfigureDataBase(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddDbContext<ApiDbContext>(options => options.UseInMemoryDatabase(configuration.GetSection("DatabaseName").Value!));
        }

        public static IServiceCollection ConfigureAppServices(this IServiceCollection services)
        {
            return services.AddScoped<IRegisterSaleAppService, RegisterSaleAppService>()
                           .AddScoped<IGetSaleAppService, GetSaleAppService>()
                           .AddScoped<IUpdateSaleStatusAppService, UpdateSaleStatusAppService>();
        }

        public static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            return services.AddScoped<IRegisterSaleService, RegisterSaleService>()
                           .AddScoped<IGetSaleService, GetSaleService>()
                           .AddScoped<IUpdateStatusService, UpdateStatusService>();
        }

        public static IServiceCollection ConfigureRepositories(this IServiceCollection services)
        {
            return services.AddScoped<IUnitOfWork, UnitOfWork>()
                           .AddScoped<ISalesRepository, SalesRepository>();
        }

        public static IServiceCollection ConfigureValidators(this IServiceCollection services)
        {
            return services.AddScoped<IValidator<RegisterSellerRequest>, RegisterSellerRequestValidator>()
                           .AddScoped<IValidator<RegisterSaleRequest>, RegisterSaleRequestValidator>()
                           .AddScoped<IValidator<SaleItemRequest>, SaleItemValidator>()
                           .AddScoped<IValidator<UpdateStatusRequest>, UpdateStatusRequestValidator>();
        }

        public static IServiceCollection ConfigureAutoMapper(this IServiceCollection services)
        {
            return services.AddAutoMapper(config => AutoMapperConfig.MapperConfig(config));
        }
    }
}
