﻿using Domain.Entities;
using Domain.Repositories;
using Infra.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace Infra.Data.Repositories
{
    public class SalesRepository(ApiDbContext context) : ISalesRepository
    {
        public async Task RegisterSale(Sale sale, CancellationToken cancellationToken) => await context.Sales.AddAsync(sale, cancellationToken);

        public async Task<Sale?> GetSale(Guid saleId, CancellationToken cancellationToken) 
            => await context.Sales.Include(s => s.Seller)
                                   .Include(s => s.Items).FirstOrDefaultAsync(s => s.Id == saleId, cancellationToken);

        public void UpdateSale(Sale sale) => context.Sales.Update(sale);
    }
}
