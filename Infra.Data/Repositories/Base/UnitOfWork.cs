﻿using Domain.Repositories.Base;
using Infra.Data.Context;

namespace Infra.Data.Repositories.Base
{
    public class UnitOfWork(ApiDbContext context) : IUnitOfWork
    {
        private readonly ApiDbContext _context = context;

        public async Task SaveChanges() => await _context.SaveChangesAsync();
    }
}
